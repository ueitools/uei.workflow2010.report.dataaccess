using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
namespace UEI.Workflow2010.Report.DataAccess
{
    public class IBatisDataAccess:IDBAccess
    {
        #region IDBAccess Members
        public void DBInit()
        {
        }
        public void DBInit(String strConnectionString)
        {
            DAOFactory.ResetDBConnection(strConnectionString);
        }
        public IList<String> GetAllIDList()
        {
           
            IList<String> allIDList = new List<String>();
            using (new ConnectionSwitcher())
            {
                StringCollection _allIDList = new StringCollection();
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allIDList = _wrapper.GetAllIDList();
                foreach (String _id in _allIDList)
                    allIDList.Add(_id);
            }
            return allIDList;
        }
        /// <summary>
        /// Gets the ID list based on the inputs in the IDSearchParameter object.
        /// </summary>
        /// <remarks>Added by binil on 12/10/2010.Implemented stored procedure for getting ids</remarks>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public IList<string> GetAllIDList(IDSearchParameter parameter)
        {
            IList<String> allIDList = new List<String>();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                allIDList = wrapper.GetAllIDList(parameter);
                if (allIDList.Contains("--"))
                    allIDList.Remove("--");
            }
            return allIDList;
        }
        public IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param)
        {
            
            IDBrandResultCollection idByRegions = new IDBrandResultCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                idByRegions = _wrapper.GetIDByRegionResults(m_param);
            }
            return idByRegions;
        }
        /// <summary>
        /// Gets all the available modes from database.
        /// </summary>
        /// <remarks>Added on 18/10/2010 by binil.</remarks>
        /// <returns></returns>
        public IList<string> GetModeList()
        {
            IList<string> modeList = null;
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                modeList = wrapper.GetModeList();
            }
            return modeList;
        }
        //public CountryCollection GetAllCountryListByRegion(StringCollection regionList)
        //{
        //    CountryCollection _allCountryList = new CountryCollection();
        //    using (new ConnectionSwitcher())
        //    {
        //        IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
        //        _allCountryList = _wrapper.GetAllCountryListByRegion(regionList);
        //    }
        //    return _allCountryList;
        //}

        public CountryCollection GetAllCountryListByRegion(IDSearchParameter param)
        {
            CountryCollection _allCountryList = new CountryCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allCountryList = _wrapper.GetAllCountryListByRegion(param);
            }
            return _allCountryList;
        }
        //Added by binil on May 09,2012
        public DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            DeviceTypeCollection _deviceList = new DeviceTypeCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();

                _deviceList = _wrapper.GetCompleteDevices(param);
            }
            return _deviceList;

        }
        public DeviceTypeCollection GetDeviceTypeList(IDSearchParameter param)
        {
            DeviceTypeCollection _deviceList = new DeviceTypeCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();

                _deviceList = _wrapper.GetAllDeviceTypeList(param);
            }
            return _deviceList;
        }
        public DeviceTypeCollection GetSubDeviceTypeList(IDSearchParameter param)
        {
            DeviceTypeCollection _deviceList = new DeviceTypeCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();

                _deviceList = _wrapper.GetAllSubDeviceTypeList(param);
            }
            return _deviceList;
        }
        public DeviceTypeCollection GetComponentList(IDSearchParameter param)
        {
            DeviceTypeCollection _deviceList = new DeviceTypeCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();

                _deviceList = _wrapper.GetAllComponentList(param);
            }
            return _deviceList;
        }
        public CountryCollection GetAllRegionList()
        {
            CountryCollection _allRegionList = new CountryCollection();
            using (new ConnectionSwitcher())
            {
                
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allRegionList = _wrapper.GetAllRegionList();
            }
            return _allRegionList;
        }
        public CountryCollection GetAllSubRegionList(IDSearchParameter param)
        {
            CountryCollection _allSubRegionsList = new CountryCollection();
            using (new ConnectionSwitcher())
            {

                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allSubRegionsList = _wrapper.GetAllSubRegionList(param);
            }
            return _allSubRegionsList;
        }        
        public StringCollection GetAllIDListByRegionCountry(IDSearchParameter param)
        {
            StringCollection _allIDS = new StringCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allIDS = _wrapper.GetAllIDList();
            }
            return _allIDS;
        }
        public StringCollection GetAllDataSources()
        {
            StringCollection _allDataSourceLocations = new StringCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();
                _allDataSourceLocations = _wrapper.GetAllDataSources();
            }
            return _allDataSourceLocations;
        }
        /// <summary>
        /// Gets the ID/Brand records from database.
        /// <remarks>Added by binil on 09/17/2010 for ID/Brand Report in DBM.</remarks>
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IDBrandResultCollection GetIDBrandResults(IDSearchParameter param)
        {
            IDBrandResultCollection idBrandResults = new IDBrandResultCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                idBrandResults = wrapper.GetIDBrandResults(param);
            }
            return idBrandResults;
        }
        public IDBrandResultCollection GetAllBSCIDList(IDSearchParameter param)
        {
            IDBrandResultCollection allbscIDList = new IDBrandResultCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                allbscIDList = wrapper.GetAllBSCIDList(param);
            }
            return allbscIDList;
        }

        public IDBrandResultCollection GetModelInformation(IDSearchParameter param)
        {
            IDBrandResultCollection modelInfos = new IDBrandResultCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                modelInfos = wrapper.GetModelInformation(param);
            }
            return modelInfos;
        }

        public HashtableCollection GetAllModeNames(String mode, String language)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper wrapper = new DbFunctionsWrapper();
                if(mode != null)
                    return(wrapper.GetAllModeNames(mode,language));
                else
                    return(wrapper.GetAllModeNames(null, ""));
            }
        }
        #endregion        

        #region IDBAccess Members for Locations
        public CountryCollection GetLocations()
        {
            CountryCollection _locationList = new CountryCollection();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper _wrapper = new DbFunctionsWrapper();

                _locationList = _wrapper.GetLocationList();
            }
            return _locationList;
        }
        #endregion
    
        #region IDBAccess Members for ExecIDTnInfo Report


        public List<int> GetExecutorCodes()
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();
                return (List<Int32>)dbFunctionWrapper.GetExecutorCodes();
            }
        }

        public List<string> GetIDListByExecutor(int executorCode)
        {
            List<String> idList = new List<String>();

            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();
                StringCollection idCollection = dbFunctionWrapper.GetIDListByExecutor(executorCode);

                foreach (string id in idCollection)
                {
                    idList.Add(id);
                }
            }
            return idList;
        }

        public PrefixCollection GetPrefixInfo(string id)
        {
            PrefixCollection prefixCollection = new PrefixCollection();

            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();

                prefixCollection = dbFunctionWrapper.GetPrefixColllection(id);
            }

            return prefixCollection;
        }
        public IList SelectPrefix(String id)
        {
            IList list = null;
            using(new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();
                list = dbFunctionWrapper.SelectPrefix(id);
            }
            return list;
        }
        public IList IDFunctionSelect(String id)
        {
            IList list = null;
            using(new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();
                list = dbFunctionWrapper.IDFunctionSelect(id);
            }
            return list;
        }
        public IDBrandResultCollection GetModelInfoById(string id)
        {
            IDBrandResultCollection resultCollecction = null;
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFunctionWrapper = new DbFunctionsWrapper();
                resultCollecction = dbFunctionWrapper.GetModelInfoById(id);
            }

            return resultCollecction;
            
        }

        #endregion

        #region IDBAccess Members for RestrictedID Report
        public List<string> GetRestrictedIDList()
        {
            List<String> idList = new List<string>();
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbFuncWrapper = new DbFunctionsWrapper();
                StringCollection idCollection = dbFuncWrapper.GetRestrictedIDList();

                if (idCollection != null)
                {
                    foreach (String id in idCollection)
                    {
                        idList.Add(id);
                    }
                }
                
            }
            return idList;
        }
        #endregion

        #region IDBAccess Members for QA  Status
        public StringCollection NonBlockingGetAllIDList()
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbfunction = new DbFunctionsWrapper();
                StringCollection idList = dbfunction.NonBlockingGetAllIDList();
                return idList;
            }
            return null;
        }
        public IDHeader GetHeaderData(String id)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbfunction = new DbFunctionsWrapper();
                IDHeader headerData = dbfunction.GetHeaderData(id);
                return headerData;
            }
            return null;
        }
        public IntegerCollection GetTNList(String id)
        {
            using (new ConnectionSwitcher())
            {                
                IDbFunctionsWrapper dbfunction = new DbFunctionsWrapper();
                IntegerCollection tnList = dbfunction.GetTNList(id);
                return tnList;
            }
            return null;
        }
        public TNHeader GetTnStatus(Int32 tn)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbfunction = new DbFunctionsWrapper();
                TNHeader tnHeader = dbfunction.GetTnStatus(tn);
                return tnHeader;
            }
            return null;
        }
        #endregion

        #region IDBAccess Members for Empy/NonEmpty Id Report


        public List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param)
        {
            try
            {
                if (param!=null)
                {
                    using (new ConnectionSwitcher())
                    {
                        IDbFunctionsWrapper dbFunc = new DbFunctionsWrapper();
                        return dbFunc.FilterEmptyNonEmptyIDs(param);
                    }
                }

                return null;
            }
            catch
            {
                throw;
            }
        }



        #endregion

        #region IDBAccess Members for Unique Function IR Count
        public Int32 SelectUniqueFuncCount(String mode)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper uniqueFunctionCount = new DbFunctionsWrapper();
                return uniqueFunctionCount.SelectUniqueFuncCount(mode);
            }
            return 0;
        }
        public Int32 SelectUniqueIRCount(String mode)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper uniqueIRCount = new DbFunctionsWrapper();
                return uniqueIRCount.SelectUniqueIRCount(mode);
            }
            return 0;
        }
        public HashtableCollection SelectUniqueFunctionIRCount()
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper uniquefunctionirCount = new DbFunctionsWrapper();
                return uniquefunctionirCount.SelectUniqueFunctionIRCount();
            }
        }
        #endregion

        #region IDBAccess Members for Disconnected TN
        public System.Collections.IList FindDisconnectedTNs()
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper diconnectedtns = new DbFunctionsWrapper();
                return diconnectedtns.FindDisconnectedTNs();
            }
        }
        #endregion

        #region IDBAccess Members
        public IList<String> GetRemoteImages(Int32 tnNumber)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper remoteImages = new DbFunctionsWrapper();
                return remoteImages.GetRemoteImages(tnNumber);
            }
        }
        #endregion

        #region IDBAccess Members for LabelIntronSearch report        
        public FunctionCollection IntronLabelSearch(IDSearchParameter param)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.IntronLabelSearch(param);
            }
        }
        public FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.ExcludeIntronLabelSearch(param);
            }
        }

        public HashtableCollection GetIntronLabelDictionary()
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.GetIntronLabelDictionary() ;
            }
        }

        #endregion

        #region IDBAccess Members for Brand Based Search List        
        public IDBrandResultCollection GetBrandSearchList(IDSearchParameter param)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.GetBrandSearchList(param);
            }
        }
        #endregion

        #region IDBAccess Members for Get All Brands
        public HashtableCollection GetAllBrands(IDSearchParameter param)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.GetAllBrands(param);
            }
        }

        #endregion


        #region IDBAccess Members for consolidate reports


        public Hashtable GetTotalCounts(bool totalExecutors, bool totalUsedExecutors, bool mainDevices, bool subDevices, bool deviceTypes, bool standardBrand,bool aliasBrand,bool brandVariations, bool deviceModels, bool remoteModels, bool uniqueKeyFunctions, bool keyCount)
        {
            try
            {
                Hashtable results = new Hashtable();

                using (new ConnectionSwitcher())
                {
                    IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();

                    if (totalExecutors)
                        results.Add("ExecutorCount", dbWrapper.TotalExecutors());

                    if (totalUsedExecutors)
                        results.Add("UsedExecutorCount", dbWrapper.TotalUsedExecutors());

                    if (mainDevices)
                        results.Add("MainDeviceCount", dbWrapper.TotalMainDevices());

                    if (subDevices)
                        results.Add("SubDeviceCount", dbWrapper.TotalSubDevices());

                    if (deviceTypes)
                        results.Add("DeviceTypeCount", dbWrapper.TotalDeviceTypes());

                    if (standardBrand || aliasBrand || brandVariations)
                        results.Add("BrandCount", dbWrapper.TotalBrandCounts());

                    if (deviceModels)
                        results.Add("DeviceModelCount", dbWrapper.TotalDeviceModels());

                    if (remoteModels)
                        results.Add("RemoteModelCount", dbWrapper.TotalRemoteModels());

                    if (uniqueKeyFunctions)
                        results.Add("KeyFunctionCount", dbWrapper.TotalKeyFunctions());

                    if (keyCount)
                        results.Add("KeyCount", dbWrapper.TotalKeyCount());

                }

                return results;
            }
            catch
            {
                throw;
            }
        }

        #endregion


        #region IDBAccess Members for ExecIdMap


        public HashtableCollection GetExecIdMap(IList<string> idList)
        {
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                return dbWrapper.GetExecIdMap(idList);
            }
        }

        #endregion

        #region IDBAccess Members ID Change Notification
        public HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag)
        {
            HashtableCollection htc = null;
            using(new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                htc = dbWrapper.GetUpdatedIds(strStartDate,strEndDate,intStatusFlag);
            }
            return htc;
        }
        #endregion


        public IList<string> GetDeviceGroupList()
        {
            return DBFunctions.GetDeviceGroupList();
        }

        public IList<string> GetModeListForDeviceGroup(String deviceGroup)
        {
            return DBFunctions.GetModeListForDeviceGroup(deviceGroup);
        }

        #region IDBAccess Members for Key Function        
        public HashtableCollection GetKeyFunction(IDSearchParameter param)
        {
            HashtableCollection htc = null;
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                htc = dbWrapper.GetKeyFunction(param);
            }
            return htc;
        }

        #endregion

        #region Supported Ids By Platforms
        public IList<Hashtable> GetAllSupportedPlatforms()
        {
            IList<Hashtable> htc = null;
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                htc = dbWrapper.GetAllSupportedPlatforms();
            }
            return htc;
        }

        public IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<String> platforms)
        {
            IList<Hashtable> htc = null;
            using (new ConnectionSwitcher())
            {
                IDbFunctionsWrapper dbWrapper = new DbFunctionsWrapper();
                htc = dbWrapper.GetAllSupportedIdsForPlatforms(platforms);
            }
            return htc;
        }
        #endregion

        public DataSet GetModelInformation_ArdKey(IDSearchParameter param)
        {
            return null;
        }        
    }
    #region Data Base Connection
    public enum DataBases
    {
        UEIPublic,
        UEITemp
    };
    internal class ConnectionSwitcher : IDisposable
    {
        private string _lastConn;
        public ConnectionSwitcher()
        {
            _lastConn = DAOFactory.GetDBConnectionString();
            if (_lastConn != DBConnectionString.UEIPUBLIC &&
                _lastConn != DBConnectionString.UEITEMP &&
                _lastConn != DBConnectionString.UEITEMP_INDIA)
                DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
        }
        public void ResetDBConnection(DataBases selectedDB)
        {
            switch (selectedDB)
            {
                case DataBases.UEIPublic:
                    DAOFactory.ResetDBConnection(DBConnectionString.UEIPUBLIC);
                    break;
                case DataBases.UEITemp:
                    DAOFactory.ResetDBConnection(DBConnectionString.UEITEMP);
                    break;
            }
        }
        public void Dispose()
        {
            DAOFactory.ResetDBConnection(_lastConn);
        }
    }
    #endregion
}