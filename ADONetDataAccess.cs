using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using System.Data;
using System.Configuration;
using System.Collections;

namespace UEI.Workflow2010.Report.DataAccess
{
    public class ADONetDataAccess:IDBAccess
    {
        ADONetDBOperations objdb;

        #region IDBAccess Members
        public void DBInit()
        {
            objdb = new ADONetDBOperations();
        }
        public void DBInit(String strConnectionString)
        {
            objdb = new ADONetDBOperations(strConnectionString);
        }
        public IList<String> GetAllIDList()
        {
            IList<String> allIDList = new List<String>();
            IDbCommand cmd = objdb.CreateCommand();
            String _sqlQuery = ConfigurationSettings.AppSettings["GetAllIDList"].ToString();
            cmd.CommandText = _sqlQuery;
            cmd.CommandType = CommandType.Text;
            IDataReader dr = objdb.ExecuteTextReader(cmd);
            while (dr.Read())
                allIDList.Add(dr[0].ToString());
            dr.Close();
            return allIDList;
        }

        public IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param)
        {
            IList<String> allIDList = new List<String>();           
            return null;
        }

        //Commented as part of implementation of sp on June 27
        //public CountryCollection GetAllCountryListByRegion(StringCollection regionList)
        //{           
        //    CountryCollection _allCountryList = new CountryCollection();
        //    IDbCommand cmd = objdb.CreateCommand();
        //    String _sqlQuery = ConfigurationSettings.AppSettings["GetAllCountryList"].ToString();
        //    cmd.CommandText = _sqlQuery;
        //    cmd.CommandType = CommandType.Text;
        //    IDataReader dr = objdb.ExecuteTextReader(cmd);
        //    while (dr.Read())
        //    {
        //        Country _country = new Country();
        //        //                
        //    }
        //    dr.Close();
        //    return _allCountryList;
        //}
        public CountryCollection GetAllCountryListByRegion(IDSearchParameter param)
        {
            
            return null;
        }

        public DeviceTypeCollection GetDeviceListByRegionCountry(IDSearchParameter param)
        {
            DeviceTypeCollection _deviceList = new DeviceTypeCollection();           
            return _deviceList;
        }
        public CountryCollection GetAllRegionList()
        {
            CountryCollection _allCountryList = new CountryCollection();          
            return _allCountryList;
        }
        public StringCollection GetAllIDListByRegionCountry(IDSearchParameter param)
        {
            StringCollection _allIDS = new StringCollection();
            return _allIDS;
        }
        public StringCollection GetAllDataSources()
        {
            StringCollection _allDataSourceLocations = new StringCollection();
            return _allDataSourceLocations;
        }
        /// <summary>
        /// <remarks>Added by binil on  09/17/2010 for ID/Brand report in DBM.Not yet implemented.</remarks>
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public IDBrandResultCollection GetIDBrandResults(IDSearchParameter param)
        {
            return new IDBrandResultCollection();
        }

        public IDBrandResultCollection GetAllBSCIDList(IDSearchParameter _param)
        {
            IDBrandResultCollection allbscIDList = new IDBrandResultCollection();            
            return allbscIDList;
        }

        #region Model Info Report
        public DataSet GetModelInformation_ArdKey(IDSearchParameter param)
        {
            DataSet dsResult = null;          
            IDbCommand objCmd = this.objdb.CreateCommand();
            objCmd.CommandText = "[dbo].[UEI2_Sp_GetModelReport_ARD]";
            objCmd.Parameters.Add(objdb.CreateParameter("@nvcharLoadList", DbType.String, ParameterDirection.Input, param.Ids.Length, param.Ids));
            objCmd.Parameters.Add(objdb.CreateParameter("@vcharSources", DbType.String, ParameterDirection.Input, param.DataSources.Length, param.DataSources));
            objCmd.Parameters.Add(objdb.CreateParameter("@nModelwithoutNameFilter", DbType.String, ParameterDirection.Input, 2, param.IncudeRecordWithoutModelName));
            objCmd.Parameters.Add(objdb.CreateParameter("@nCodebookFilter", DbType.String, ParameterDirection.Input, 2, param.RemoveNonCodeBookModels));
            objCmd.Parameters.Add(objdb.CreateParameter("@nTNFilter", DbType.String, ParameterDirection.Input, 2, param.TNLink));
            objCmd.Parameters.Add(objdb.CreateParameter("@nExcludeRIDs", DbType.String, ParameterDirection.Input, 2, param.IncludeRestrictedIDs));
            objCmd.Parameters.Add(objdb.CreateParameter("@nOutputFlag", DbType.String, ParameterDirection.Input, 2, param.SearchFlag));
            objCmd.Parameters.Add(objdb.CreateParameter("@nEmptyDeviceTypeFilter", DbType.String, ParameterDirection.Input, 2, param.EmptyDeviceTypeFilter));
            if (!String.IsNullOrEmpty(param.XMLLocations))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@xmlLocations", DbType.String, ParameterDirection.Input, param.XMLLocations.Length, param.XMLLocations));
            }
            if (!String.IsNullOrEmpty(param.XMLDevices))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@xmlDevices", DbType.String, ParameterDirection.Input, param.XMLDevices.Length, param.XMLDevices));
            }
            if (!String.IsNullOrEmpty(param.StartDate))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@dtStartDate", DbType.String, ParameterDirection.Input, param.StartDate.Length, param.StartDate));
                objCmd.Parameters.Add(objdb.CreateParameter("@dtEndDate", DbType.String, ParameterDirection.Input, param.EndDate.Length, param.EndDate));
            }
            if (!String.IsNullOrEmpty(param.Brands))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@nBrandFilter", DbType.String, ParameterDirection.Input, 2, param.ExcludeSelectedBrands));
                objCmd.Parameters.Add(objdb.CreateParameter("@nvcharBrands", DbType.String, ParameterDirection.Input, param.Brands.Length, param.Brands));
            }
            if (!String.IsNullOrEmpty(param.Models))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@nModelFilter", DbType.String, ParameterDirection.Input, 2, param.ExcludeSelectedModels));
                objCmd.Parameters.Add(objdb.CreateParameter("@nvcharModels", DbType.String, ParameterDirection.Input, param.Models.Length, param.Models));
            }
            if (!String.IsNullOrEmpty(param.IncludeUnknownLocation.ToString()))
            {
                objCmd.Parameters.Add(objdb.CreateParameter("@nUnknownLocationFilter", DbType.String, ParameterDirection.Input, 2, param.IncludeUnknownLocation));
            }
            objCmd.CommandType = CommandType.StoredProcedure;
            objCmd.CommandTimeout = 1800;
            dsResult = this.objdb.ExecuteDataSet(objCmd);                            
            return dsResult;
        }
        public IDBrandResultCollection GetModelInformation(IDSearchParameter param)
        {            
            return null;
        }

        #endregion
        public IList<string> GetAllIDList(IDSearchParameter parameter)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        public IList<string> GetModeList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public HashtableCollection GetAllModeNames(String mode, String language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for ExecIDTnInfo Report

        void IDBAccess.DBInit()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IList<string> IDBAccess.GetAllIDList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IList<string> IDBAccess.GetModeList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IList<string> IDBAccess.GetAllIDList(IDSearchParameter parameter)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IDBrandResultCollection IDBAccess.GetIDByRegionResults(IDSearchParameter m_param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        StringCollection IDBAccess.GetAllIDListByRegionCountry(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        CountryCollection IDBAccess.GetAllRegionList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        //CountryCollection IDBAccess.GetAllCountryListByRegion(StringCollection regionList)
        //{
        //    throw new Exception("The method or operation is not implemented.");
        //}

        StringCollection IDBAccess.GetAllDataSources()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IDBrandResultCollection IDBAccess.GetIDBrandResults(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IDBrandResultCollection IDBAccess.GetAllBSCIDList(IDSearchParameter _parama)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IDBrandResultCollection IDBAccess.GetModelInformation(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        HashtableCollection IDBAccess.GetAllModeNames(string mode, string language)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        List<int> IDBAccess.GetExecutorCodes()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        List<string> IDBAccess.GetIDListByExecutor(int executorCode)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        PrefixCollection IDBAccess.GetPrefixInfo(string id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        IDBrandResultCollection IDBAccess.GetModelInfoById(string id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for RestrictedID Report


        public List<string> GetRestrictedIDList()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for QA Status
        public StringCollection NonBlockingGetAllIDList()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public IDHeader GetHeaderData(String id)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public IntegerCollection GetTNList(String id)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public TNHeader GetTnStatus(Int32 tn)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for EmptyNonEmptyIDReport
        public List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for Unique Function and IR Count
        public int SelectUniqueFuncCount(string mode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public int SelectUniqueIRCount(string mode)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members for disconnected TN
        public System.Collections.IList FindDisconnectedTNs()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members for Remote Images
        public IList<string> GetRemoteImages(int tnNumber)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members For Unique Function IR Count
        public HashtableCollection SelectUniqueFunctionIRCount()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members for IntronLabelSearch report


        public FunctionCollection IntronLabelSearch(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public HashtableCollection GetIntronLabelDictionary()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for Brand Based Search List
        public IDBrandResultCollection GetBrandSearchList(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion


        #region IDBAccess Members for Get All Brands
        public HashtableCollection GetAllBrands(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members


        public CountryCollection GetAllSubRegionList(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for Devices, Sub Devices and Components
        public DeviceTypeCollection GetCompleteDevices(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
}
        public DeviceTypeCollection GetDeviceTypeList(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public DeviceTypeCollection GetSubDeviceTypeList(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        public DeviceTypeCollection GetComponentList(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members for Locations        
        public CountryCollection GetLocations()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for ExecIdMap


        public HashtableCollection GetExecIdMap(IList<string> idList)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDBAccess Members


        public Hashtable GetTotalCounts(bool totalExecutors, bool totalUsedExecutors, bool mainDevices, bool subDevices, bool deviceTypes, bool standardBrand,bool aliasBrand,bool brandVariations, bool deviceModels, bool remoteModels, bool uniqueKeyFunctions, bool keyCount)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion

        #region IDBAccess Members for Select Prefix
        public IList SelectPrefix(string id)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members for ID Function Select
        public IList IDFunctionSelect(string id)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion

        #region IDBAccess Members


        public HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        public IList<string> GetDeviceGroupList()
        {
            throw new NotImplementedException();
        }

        public IList<string> GetModeListForDeviceGroup(string deviceGroup)
        {
            throw new NotImplementedException();
        }

        #region IDBAccess Members for Key Function Report
        public HashtableCollection GetKeyFunction(IDSearchParameter param)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Supported Ids By Platform
        public IList<Hashtable> GetAllSupportedPlatforms()
        {
            throw new NotImplementedException();
        }
        public IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<string> platforms)
        {
            throw new NotImplementedException();
        }
        #endregion


        public FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param)
        {
            throw new Exception("The method or operation is not implemented.");
        }
    }
}
