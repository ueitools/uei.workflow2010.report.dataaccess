using System;
using System.Collections.Generic;
using System.Text;
using BusinessObject;
using System.Collections;
using System.Data;

namespace UEI.Workflow2010.Report.DataAccess
{
    public interface IDBAccess
    {        
        void DBInit();
        void DBInit(String strConnectionString);
        IList<String> GetAllIDList();
        IList<String> GetModeList();
        IList<String> GetAllIDList(IDSearchParameter parameter);
        IDBrandResultCollection GetIDByRegionResults(IDSearchParameter m_param);
        StringCollection GetAllIDListByRegionCountry(IDSearchParameter param);
        CountryCollection GetAllRegionList();
        CountryCollection GetLocations();
        CountryCollection GetAllSubRegionList(IDSearchParameter param);
        CountryCollection GetAllCountryListByRegion(IDSearchParameter param);
        DeviceTypeCollection GetCompleteDevices(IDSearchParameter param);
        DeviceTypeCollection GetDeviceTypeList(IDSearchParameter param);
        DeviceTypeCollection GetSubDeviceTypeList(IDSearchParameter param);
        DeviceTypeCollection GetComponentList(IDSearchParameter param);
        StringCollection GetAllDataSources();
        IDBrandResultCollection GetIDBrandResults(IDSearchParameter param);                
        IDBrandResultCollection GetAllBSCIDList(IDSearchParameter _parama);
        IDBrandResultCollection GetModelInformation(IDSearchParameter param);
        DataSet GetModelInformation_ArdKey(IDSearchParameter param);
        HashtableCollection GetAllModeNames(String mode, String language);
        HashtableCollection GetKeyFunction(IDSearchParameter param);        
        IList<String> GetDeviceGroupList();
        IList<String> GetModeListForDeviceGroup(String deviceGroup);
        List<Int32> GetExecutorCodes();
        List<String> GetIDListByExecutor(Int32 executorCode);
        PrefixCollection GetPrefixInfo(String id);
        IList SelectPrefix(String id);
        IList IDFunctionSelect(String id);
        IDBrandResultCollection GetModelInfoById(String id);
        List<String> GetRestrictedIDList();
        StringCollection NonBlockingGetAllIDList();
        IDHeader GetHeaderData(String id);
        IntegerCollection GetTNList(String id);
        TNHeader GetTnStatus(Int32 tn);
        List<String> FilterEmptyNonEmptyIDs(IDSearchParameter param);
        Int32 SelectUniqueFuncCount(String mode);
        Int32 SelectUniqueIRCount(String mode);
        IList FindDisconnectedTNs();
        HashtableCollection SelectUniqueFunctionIRCount();
        IList<String> GetRemoteImages(Int32 tnNumber);
        FunctionCollection IntronLabelSearch(IDSearchParameter param);
        FunctionCollection ExcludeIntronLabelSearch(IDSearchParameter param);
        HashtableCollection GetIntronLabelDictionary();
        IDBrandResultCollection GetBrandSearchList(IDSearchParameter param);
        HashtableCollection GetAllBrands(IDSearchParameter param);
        Hashtable GetTotalCounts(bool totalExecutors,bool totalUsedExecutors,bool mainDevices,bool subDevices,bool deviceTypes, bool standardBrand,bool aliasBrand,bool brandVariations,
        bool deviceModels,bool remoteModels,bool uniqueKeyFunctions,bool keyCount);
        HashtableCollection GetExecIdMap(IList<string> idList);
        HashtableCollection GetUpdatedIds(String strStartDate, String strEndDate, Int32 intStatusFlag);
        IList<Hashtable> GetAllSupportedPlatforms();
        IList<Hashtable> GetAllSupportedIdsForPlatforms(IList<String> platforms);
    }
}
