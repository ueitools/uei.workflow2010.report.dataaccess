using System;
using System.Collections.Generic;
using System.Text;

namespace UEI.Workflow2010.Report.DataAccess
{
    //Data Access Layer Types
    public enum DBLayerType
    {
        IBatis,
        ADONet
    }
    public class DBFactory
    {
        private DBLayerType _DBLayerType;
        public DBLayerType LayerType{
            get{
                return _DBLayerType;
            }
            set{
                _DBLayerType = value;
            }
        }

        public IDBAccess GetDBAccessLayerType()
        {
            IDBAccess _DBAccess = null;
            switch (LayerType)
            {
                case DBLayerType.IBatis:
                    _DBAccess = new IBatisDataAccess() as IDBAccess;
                    break;
                case DBLayerType.ADONet:
                    _DBAccess = new ADONetDataAccess() as IDBAccess;
                    break;
            }
            return _DBAccess;
        }
        public IDBAccess GetDBAccessLayerType(DBLayerType _LayerType)
        {
            IDBAccess _DBAccess = null;
            switch (_LayerType)
            {
                case DBLayerType.IBatis:
                    _DBAccess = new IBatisDataAccess() as IDBAccess;
                    break;
                case DBLayerType.ADONet:
                    _DBAccess = new ADONetDataAccess() as IDBAccess;
                    break;
            }
            return _DBAccess;
        }
    }
}
